package controllers

import (
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	"github.com/gammazero/workerpool"
	"gitlab.com/BIC_Dev/nitrado-api-client/client/service"
	"gitlab.com/BIC_Dev/nitrado-service-v3/utils/logging"
	"gitlab.com/BIC_Dev/nitrado-service-v3/viewmodels"
	"go.uber.org/zap"
)

func (c *Controller) GetAllServices(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	var body viewmodels.GetAllServicesRequest
	if err := json.NewDecoder(r.Body).Decode(&body); err != nil {
		Error(ctx, w, "Failed to decode BanPlayer request body", err, http.StatusBadRequest)
		return
	}

	wp := workerpool.New(5)
	defer wp.Stop()

	successChannel := make(chan viewmodels.GetAllServiceServices)
	errorChannel := make(chan viewmodels.FailedService)
	defer close(successChannel)
	defer close(errorChannel)

	for _, token := range body.Tokens {
		params := service.NewGetServicesParamsWithTimeout(c.Config.NitradoClient.Timeout * time.Second)

		token := token

		wp.Submit(func() {
			resp, err := c.NitradoAPIClient.Service.GetServices(params, NitradoBearerToken(token.Value))
			if err != nil {
				errorChannel <- viewmodels.FailedService{
					TokenID:       token.ID,
					FailureReason: err.Error(),
				}

			} else {
				successChannel <- viewmodels.GetAllServiceServices{
					TokenID:  token.ID,
					Services: resp.Payload.Data.Services,
				}
			}
		})
	}

	nitradoServices := make([]viewmodels.GetAllServiceServices, 0)
	failedRequests := make([]viewmodels.FailedService, 0)
	totalToProcess := len(body.Tokens)

	for {
		select {
		case services := <-successChannel:
			nitradoServices = append(nitradoServices, services)
			totalToProcess -= 1
		case failure := <-errorChannel:
			failedRequests = append(failedRequests, failure)
			totalToProcess -= 1
		}

		if totalToProcess <= 0 {
			break
		}
	}

	status := http.StatusOK
	if len(nitradoServices) == 0 {
		status = http.StatusBadRequest
	}

	Response(ctx, w, viewmodels.GetAllServicesResponse{
		Message:         fmt.Sprintf("Retrieved %d services across %d accounts", len(nitradoServices), len(body.Tokens)),
		AccountServices: nitradoServices,
		FailedServices:  failedRequests,
	}, status)
}
