package controllers

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strings"
	"time"

	"github.com/gammazero/workerpool"
	"gitlab.com/BIC_Dev/nitrado-api-client/client/player_management"
	"gitlab.com/BIC_Dev/nitrado-service-v3/utils/cache"
	"gitlab.com/BIC_Dev/nitrado-service-v3/utils/logging"
	"gitlab.com/BIC_Dev/nitrado-service-v3/viewmodels"
	"go.uber.org/zap"
)

// Get players matching name
func (c *Controller) PlayerSearch(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	var body viewmodels.PlayerSearchRequest
	if err := json.NewDecoder(r.Body).Decode(&body); err != nil {
		Error(ctx, w, "Failed to decode GetPlayer request body", err, http.StatusBadRequest)
		return
	}

	var matchingPlayers []viewmodels.Player = make([]viewmodels.Player, 0)

	for _, server := range body.Servers {
		var serverPlayers cache.ServerPlayers

		if err := GetCache(ctx, c.Cache, c.Config.CacheSettings.ServerPlayers, server.ID, &serverPlayers); err != nil {
			Error(ctx, w, err.Message, err.Err, http.StatusBadRequest)
			return
		}

		for _, player := range serverPlayers.Players {
			if strings.Contains(player.Name, body.Name) && (player.Online == "true" || !body.Online) {
				matchingPlayers = append(matchingPlayers, viewmodels.Player{
					Name:         player.Name,
					ID:           player.ID,
					IDType:       player.IDType,
					Online:       player.Online,
					LastServerID: server.ID,
				})
			}
		}
	}

	Response(ctx, w, viewmodels.PlayerSearchResponse{
		Message: fmt.Sprintf("Found %d players matching %s", len(matchingPlayers), body.Name),
		Players: matchingPlayers,
	}, http.StatusOK)
}

func (c *Controller) PlayerBan(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	var body viewmodels.PlayerBanRequest
	if err := json.NewDecoder(r.Body).Decode(&body); err != nil {
		Error(ctx, w, "Failed to decode BanPlayer request body", err, http.StatusBadRequest)
		return
	}

	wp := workerpool.New(5)

	successChannel := make(chan string)
	errorChannel := make(chan viewmodels.FailedServers)
	defer close(successChannel)
	defer close(errorChannel)

	for _, server := range body.Servers {
		params := player_management.NewBanPlayerParamsWithTimeout(c.Config.NitradoClient.Timeout * time.Second)
		params.ServiceID = server.ID
		params.Body.Identifier = body.Name

		server := server

		wp.Submit(func() {
			_, err := c.NitradoAPIClient.PlayerManagement.BanPlayer(params, NitradoBearerToken(server.Token.Value))
			if err != nil {
				errorChannel <- viewmodels.FailedServers{
					ServiceID:     params.ServiceID,
					FailureReason: err.Error(),
				}

			} else {
				successChannel <- params.ServiceID
			}
		})
	}

	var successServerIDs []string = make([]string, 0)
	var failedServers []viewmodels.FailedServers = make([]viewmodels.FailedServers, 0)
	totalToProcess := len(body.Servers)

	for {
		select {
		case serverID := <-successChannel:
			successServerIDs = append(successServerIDs, serverID)
			totalToProcess -= 1
		case failure := <-errorChannel:
			failedServers = append(failedServers, failure)
			totalToProcess -= 1
		}

		if totalToProcess <= 0 {
			break
		}
	}

	wp.Stop()

	status := http.StatusOK
	if len(successServerIDs) == 0 {
		status = http.StatusBadRequest
	}

	Response(ctx, w, viewmodels.PlayerBanResponse{
		Message:          fmt.Sprintf("Banned %s on %d/%d servers", body.Name, len(successServerIDs), len(body.Servers)),
		SuccessServerIDs: successServerIDs,
		FailedServers:    failedServers,
	}, status)
}

func (c *Controller) PlayerUnban(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	var body viewmodels.PlayerUnbanRequest
	if err := json.NewDecoder(r.Body).Decode(&body); err != nil {
		Error(ctx, w, "Failed to decode UnbanPlayer request body", err, http.StatusBadRequest)
		return
	}

	wp := workerpool.New(5)

	successChannel := make(chan string)
	errorChannel := make(chan viewmodels.FailedServers)

	for _, server := range body.Servers {
		params := player_management.NewUnbanPlayerParamsWithTimeout(c.Config.NitradoClient.Timeout * time.Second)
		params.ServiceID = server.ID
		params.Body.Identifier = body.Name

		server := server

		wp.Submit(func() {
			_, err := c.NitradoAPIClient.PlayerManagement.UnbanPlayer(params, NitradoBearerToken(server.Token.Value))
			if err != nil {
				errorChannel <- viewmodels.FailedServers{
					ServiceID:     params.ServiceID,
					FailureReason: err.Error(),
				}
			} else {
				successChannel <- params.ServiceID
			}
		})
	}

	var successServerIDs []string = make([]string, 0)
	var failedServers []viewmodels.FailedServers = make([]viewmodels.FailedServers, 0)
	totalToProcess := len(body.Servers)

	for {
		select {
		case serverID := <-successChannel:
			successServerIDs = append(successServerIDs, serverID)
			totalToProcess -= 1
		case failure := <-errorChannel:
			failedServers = append(failedServers, failure)
			totalToProcess -= 1
		}

		if totalToProcess <= 0 {
			break
		}
	}

	wp.Stop()

	status := http.StatusOK
	if len(successServerIDs) == 0 {
		status = http.StatusBadRequest
	}

	Response(ctx, w, viewmodels.PlayerUnbanResponse{
		Message:          fmt.Sprintf("Banned %s on %d/%d servers", body.Name, len(successServerIDs), len(body.Servers)),
		SuccessServerIDs: successServerIDs,
		FailedServers:    failedServers,
	}, status)
}
