package controllers

import (
	"encoding/json"
	"net/http"
	"time"

	"gitlab.com/BIC_Dev/nitrado-api-client/client/o_auth_2"
	"gitlab.com/BIC_Dev/nitrado-service-v3/utils/logging"
	"gitlab.com/BIC_Dev/nitrado-service-v3/viewmodels"
	"go.uber.org/zap"
)

func (c *Controller) ValidateToken(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	var body viewmodels.ValidateTokenRequest
	if err := json.NewDecoder(r.Body).Decode(&body); err != nil {
		Error(ctx, w, "Failed to decode ValidateToken request body", err, http.StatusBadRequest)
		return
	}

	newTokenParams := o_auth_2.NewGetTokenInfoParamsWithTimeout(c.Config.NitradoClient.Timeout * time.Second)
	newTokenResp, newTokenErr := c.NitradoAPIClient.OAuth2.GetTokenInfo(newTokenParams, NitradoBearerToken(body.NewToken))
	if newTokenErr != nil {
		Error(ctx, w, "Invalid Nitrado long-life token provided", newTokenErr, http.StatusBadRequest)
		return
	}

	var duplicateToken viewmodels.Token
	for _, token := range body.ExistingTokens {
		existingTokenParams := o_auth_2.NewGetTokenInfoParamsWithTimeout(c.Config.NitradoClient.Timeout * time.Second)
		existingTokenResp, existingTokenErr := c.NitradoAPIClient.OAuth2.GetTokenInfo(existingTokenParams, NitradoBearerToken(token.Value))
		if existingTokenErr != nil {
			continue
		}

		if existingTokenResp.Payload.Data.Token.User.ID == newTokenResp.Payload.Data.Token.User.ID {
			duplicateToken = viewmodels.Token{
				ID:    token.ID,
				Value: token.Value,
			}
		}
	}

	Response(ctx, w, viewmodels.ValidateTokenResponse{
		Message:        "Token is valid",
		IsValid:        true,
		DuplicateToken: duplicateToken,
	}, http.StatusOK)
}
