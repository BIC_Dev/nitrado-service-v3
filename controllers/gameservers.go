package controllers

import (
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	"github.com/gorilla/mux"
	"gitlab.com/BIC_Dev/nitrado-api-client/client/gameserver"
	"gitlab.com/BIC_Dev/nitrado-service-v3/utils/logging"
	"gitlab.com/BIC_Dev/nitrado-service-v3/viewmodels"
	"go.uber.org/zap"
)

func (c *Controller) GetGameserver(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	vars := mux.Vars(r)
	gameserverID := vars["gameserver_id"]

	var body viewmodels.GetGameserverRequest
	if err := json.NewDecoder(r.Body).Decode(&body); err != nil {
		Error(ctx, w, "Failed to decode GetGameserver request body", err, http.StatusBadRequest)
		return
	}

	params := gameserver.NewGetGameserverParamsWithTimeout(c.Config.NitradoClient.Timeout * time.Second)
	params.SetServiceID(gameserverID)

	resp, err := c.NitradoAPIClient.Gameserver.GetGameserver(params, NitradoBearerToken(body.Token.Value))

	if err != nil {
		Error(ctx, w, "Failed to retrieve gameserver details", err, http.StatusBadRequest)
		return
	}

	Response(ctx, w, viewmodels.GetGameserverResponse{
		Message:    fmt.Sprintf("Retrieved gameserver %s", gameserverID),
		Gameserver: resp.Payload.Data.Gameserver,
	}, http.StatusOK)
}

func (c *Controller) StopGameserver(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	vars := mux.Vars(r)
	gameserverID := vars["gameserver_id"]

	var body viewmodels.StopGameserverRequest
	if err := json.NewDecoder(r.Body).Decode(&body); err != nil {
		Error(ctx, w, "Failed to decode StopGameserver request body", err, http.StatusBadRequest)
		return
	}

	params := gameserver.NewStopGameserverParamsWithTimeout(c.Config.NitradoClient.Timeout * time.Second)
	params.SetServiceID(gameserverID)
	params.SetBody(gameserver.StopGameserverBody{
		Message:     fmt.Sprintf("Gameserver stopped by Nitrado Service V3 on behalf of: %s (%s)", body.User.Name, body.User.ID),
		StopMessage: body.Message,
	})

	_, err := c.NitradoAPIClient.Gameserver.StopGameserver(params, NitradoBearerToken(body.Token.Value))

	if err != nil {
		Error(ctx, w, "Failed to stop gameserver", err, http.StatusBadRequest)
		return
	}

	Response(ctx, w, viewmodels.StopGameserverResponse{
		Message: fmt.Sprintf("Stopped gameserver %s", gameserverID),
	}, http.StatusOK)
}

func (c *Controller) RestartGameserver(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	vars := mux.Vars(r)
	gameserverID := vars["gameserver_id"]

	var body viewmodels.RestartGameserverRequest
	if err := json.NewDecoder(r.Body).Decode(&body); err != nil {
		Error(ctx, w, "Failed to decode RestartGameserver request body", err, http.StatusBadRequest)
		return
	}

	params := gameserver.NewRestartGameserverParamsWithTimeout(c.Config.NitradoClient.Timeout * time.Second)
	params.SetServiceID(gameserverID)
	params.SetBody(gameserver.RestartGameserverBody{
		Message:        fmt.Sprintf("Gameserver restarted by Nitrado Service V3 on behalf of: %s (%s)", body.User.Name, body.User.ID),
		RestartMessage: body.Message,
	})

	_, err := c.NitradoAPIClient.Gameserver.RestartGameserver(params, NitradoBearerToken(body.Token.Value))

	if err != nil {
		Error(ctx, w, "Failed to restart gameserver", err, http.StatusBadRequest)
		return
	}

	Response(ctx, w, viewmodels.StopGameserverResponse{
		Message: fmt.Sprintf("Restarted gameserver %s", gameserverID),
	}, http.StatusOK)
}
