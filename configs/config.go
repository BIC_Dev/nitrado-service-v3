package configs

import (
	"context"
	"os"
	"time"

	"gitlab.com/BIC_Dev/nitrado-service-v3/utils/logging"
	"go.uber.org/zap"
	"gopkg.in/yaml.v2"
)

// Config struct that contians the structure of the config
type Config struct {
	Redis struct {
		Host   string `yaml:"host"`
		Port   int    `yaml:"port"`
		Pool   int    `yaml:"pool"`
		Prefix string `yaml:"prefix"`
	} `yaml:"REDIS"`
	DB struct {
		Type string `yaml:"type"`
	} `yaml:"DB"`
	SQLite3 struct {
		Path   string `yaml:"path"`
		DBName string `yaml:"db_name"`
	} `yaml:"SQLITE3"`
	PostgreSQL struct {
		Host   string `yaml:"host"`
		Port   string `yaml:"port"`
		DBName string `yaml:"db_name"`
	} `yaml:"POSTGRESQL"`
	CacheSettings struct {
		ServerPlayers CacheSetting `yaml:"server_players"`
	} `yaml:"CACHE_SETTINGS"`
	NitradoClient struct {
		Timeout time.Duration `yaml:"timeout"`
	} `yaml:"NITRADO_CLIENT"`
}

// CacheSetting struct
type CacheSetting struct {
	Base    string `yaml:"base"`
	TTL     string `yaml:"ttl"`
	Enabled bool   `yaml:"enabled"`
}

// GetConfig gets the config file and returns a Config struct
func GetConfig(ctx context.Context, env string) *Config {
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))

	configFile := "./configs/conf-" + env + ".yml"
	f, err := os.Open(configFile)

	if err != nil {
		ctx = logging.AddValues(ctx, zap.NamedError("error", err))
		logger := logging.Logger(ctx)
		logger.Fatal("error_log")
	}

	defer f.Close()

	var config Config
	decoder := yaml.NewDecoder(f)
	err = decoder.Decode(&config)

	if err != nil {
		ctx = logging.AddValues(ctx, zap.NamedError("error", err))
		logger := logging.Logger(ctx)
		logger.Fatal("error_log")
	}

	return &config
}
