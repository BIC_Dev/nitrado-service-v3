export ENVIRONMENT=$(echo ${ENV_VARS} | jq -r '.ENVIRONMENT')
export LISTENER_PORT=$(echo ${ENV_VARS} | jq -r '.LISTENER_PORT')
export SERVICE_TOKEN=$(echo ${ENV_VARS} | jq -r '.SERVICE_TOKEN')
export BASE_PATH=$(echo ${ENV_VARS} | jq -r '.BASE_PATH')
export NITRADO_SERVICE_HOST=$(echo ${ENV_VARS} | jq -r '.NITRADO_SERVICE_HOST')
export NITRADO_SERVICE_BASEPATH=$(echo ${ENV_VARS} | jq -r '.NITRADO_SERVICE_BASEPATH')

/main