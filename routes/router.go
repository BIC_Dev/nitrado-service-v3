package routes

import (
	"context"
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/BIC_Dev/nitrado-service-v3/controllers"
	"gitlab.com/BIC_Dev/nitrado-service-v3/utils/logging"
	"go.uber.org/zap"
)

// Router struct
type Router struct {
	Controller   *controllers.Controller
	ServiceToken string
	Port         string
	BasePath     string
}

// GetRouter creates and returns a router
func GetRouter(ctx context.Context) *mux.Router {
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))
	return mux.NewRouter().StrictSlash(true)
}

// AddRoutes adds all necessary routes to the router
func AddRoutes(ctx context.Context, router *mux.Router, r *Router) {
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))
	auth := Authentication{
		ServiceToken: r.ServiceToken,
		BasePath:     r.BasePath,
	}

	// STATUS
	router.HandleFunc(r.BasePath+"/status", r.Controller.GetStatus).Methods("GET")

	// PLAYERS
	router.HandleFunc(r.BasePath+"/players/search", r.Controller.PlayerSearch).Methods("POST")
	router.HandleFunc(r.BasePath+"/players/ban", r.Controller.PlayerBan).Methods("POST")
	router.HandleFunc(r.BasePath+"/players/unban", r.Controller.PlayerUnban).Methods("POST")

	// SERVICES
	router.HandleFunc(r.BasePath+"/services", r.Controller.GetAllServices).Methods("POST")

	// GAMESERVERS
	router.HandleFunc(r.BasePath+"/gameservers/{gameserver_id}", r.Controller.GetGameserver).Methods("POST")
	router.HandleFunc(r.BasePath+"/gameservers/{gameserver_id}/stop", r.Controller.StopGameserver).Methods("POST")
	router.HandleFunc(r.BasePath+"/gameservers/{gameserver_id}/restart", r.Controller.RestartGameserver).Methods("POST")

	// TOKENS
	router.HandleFunc(r.BasePath+"/tokens/validate", r.Controller.ValidateToken).Methods("POST")

	router.Use(auth.AuthenticationMiddleware)

	loggingMiddleware := LoggingMiddleware()
	loggedRouter := loggingMiddleware(router)

	logger := logging.Logger(ctx)
	logger.Info("Starting Listener", zap.String("port", r.Port))
	if err := http.ListenAndServe(":"+r.Port, loggedRouter); err != nil {
		logger.Fatal("error_log", zap.NamedError("err", err))
	}
}
