package viewmodels

import "gitlab.com/BIC_Dev/nitrado-api-client/client/gameserver"

type Server struct {
	ID    string `json:"id"`
	Token Token  `json:"token"`
}

// GetGameserverRequest struct
type GetGameserverRequest struct {
	Token Token `json:"token"`
}

// GetGameserverResponse struct
type GetGameserverResponse struct {
	Message    string                                        `json:"message"`
	Gameserver *gameserver.GetGameserverOKBodyDataGameserver `json:"gameserver"`
}

// StopGameserverRequest struct
type StopGameserverRequest struct {
	Message string `json:"message"`
	Token   Token  `json:"token"`
	User    User   `json:"user"`
}

// StopGameserverResponse struct
type StopGameserverResponse struct {
	Message string `json:"message"`
}

// RestartGameserverRequest struct
type RestartGameserverRequest struct {
	Message string `json:"message"`
	Token   Token  `json:"token"`
	User    User   `json:"user"`
}

// RestartGameserverResponse struct
type RestartGameserverResponse struct {
	Message string `json:"message"`
}
