package viewmodels

import "gitlab.com/BIC_Dev/nitrado-api-client/client/service"

// FailedServices
type FailedServers struct {
	ServiceID     string `json:"service_id"`
	FailureReason string `json:"failure_reason"`
}

// FailedService
type FailedService struct {
	TokenID       string `json:"token_id"`
	FailureReason string `json:"failure_reason"`
}

// GetAllServicesRequest struct
type GetAllServicesRequest struct {
	Tokens []Token `json:"tokens"`
}

// GetAllServicesResponse struct
type GetAllServicesResponse struct {
	Message         string                  `json:"message"`
	AccountServices []GetAllServiceServices `json:"account_services"`
	FailedServices  []FailedService         `json:"failed_services"`
}

type GetAllServiceServices struct {
	TokenID  string                                         `json:"token_id"`
	Services []*service.GetServicesOKBodyDataServicesItems0 `json:"services"`
}
