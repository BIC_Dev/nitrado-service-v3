package viewmodels

// User struct
type User struct {
	ID   string `json:"id"`
	Name string `json:"name"`
}
