package viewmodels

type Token struct {
	ID    string `json:"id"`
	Value string `json:"value"`
}

// ValidateTokenRequest struct
type ValidateTokenRequest struct {
	NewToken       string  `json:"new_token"`
	ExistingTokens []Token `json:"existing_tokens"`
}

// ValidateTokenResponse struct
type ValidateTokenResponse struct {
	Message        string `json:"message"`
	IsValid        bool   `json:"is_valid"`
	DuplicateToken Token  `json:"duplicate_token"`
}
