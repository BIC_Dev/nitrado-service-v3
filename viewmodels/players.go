package viewmodels

type Player struct {
	Name         string `json:"name"`
	ID           string `json:"id"`
	IDType       string `json:"id_type"`
	Online       string `json:"online"`
	LastServerID string `json:"last_server_id"`
}

// PlayerSearchRequest struct
type PlayerSearchRequest struct {
	Name    string   `json:"name"`
	Servers []Server `json:"servers"`
	Online  bool     `json:"online"`
}

// PlayerSearchResponse struct
type PlayerSearchResponse struct {
	Message string   `json:"message"`
	Players []Player `json:"players"`
}

// PlayerBanRequest struct
type PlayerBanRequest struct {
	Name    string   `json:"name"`
	Servers []Server `json:"servers"`
}

// PlayerBanResponse struct
type PlayerBanResponse struct {
	Message          string          `json:"message"`
	SuccessServerIDs []string        `json:"success_server_ids"`
	FailedServers    []FailedServers `json:"failed_servers"`
}

// PlayerUnbanRequest struct
type PlayerUnbanRequest struct {
	Name    string   `json:"name"`
	Servers []Server `json:"servers"`
}

// PlayerUnbanResponse struct
type PlayerUnbanResponse struct {
	Message          string          `json:"message"`
	SuccessServerIDs []string        `json:"success_server_ids"`
	FailedServers    []FailedServers `json:"failed_servers"`
}
