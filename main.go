package main

import (
	"context"
	"log"

	"github.com/caarlos0/env"
	"github.com/go-openapi/strfmt"
	nitradoclient "gitlab.com/BIC_Dev/nitrado-api-client/client"
	"gitlab.com/BIC_Dev/nitrado-service-v3/configs"
	"gitlab.com/BIC_Dev/nitrado-service-v3/controllers"
	"gitlab.com/BIC_Dev/nitrado-service-v3/routes"
	"gitlab.com/BIC_Dev/nitrado-service-v3/utils/cache"
	"gitlab.com/BIC_Dev/nitrado-service-v3/utils/db"
	"gitlab.com/BIC_Dev/nitrado-service-v3/utils/logging"
	"go.uber.org/zap"
	"gorm.io/gorm"
)

// EnvVars struct
type EnvVars struct {
	Environment            string `env:"ENVIRONMENT,required"`
	ListenerPort           string `env:"LISTENER_PORT,required"`
	ServiceToken           string `env:"SERVICE_TOKEN,required"`
	BasePath               string `env:"BASE_PATH,required"`
	NitradoServiceHost     string `env:"NITRADO_SERVICE_HOST,required"`
	NitradoServiceBasepath string `env:"NITRADO_SERVICE_BASEPATH,required"`
	Migrate                bool   `env:"MIGRATE"`
}

func main() {
	ctx := context.Background()
	envVars := EnvVars{}
	if err := env.Parse(&envVars); err != nil {
		log.Fatal("FAILED TO LOAD ENV VARS")
	}

	ctx = logging.AddValues(ctx,
		zap.String("scope", logging.GetFuncName()),
		zap.String("env", envVars.Environment),
		zap.String("listener_port", envVars.ListenerPort),
		zap.String("base_path", envVars.BasePath),
	)

	config := configs.GetConfig(ctx, envVars.Environment)
	cache := InitCache(ctx, config)

	//transport := nitradoclient.New(envVars.NitradoServiceHost, envVars.NitradoServiceBasepath, nil)
	transport := nitradoclient.TransportConfig{
		Host:     envVars.NitradoServiceHost,
		BasePath: envVars.NitradoServiceBasepath,
		Schemes:  nil,
	}

	nitradoClient := nitradoclient.NewHTTPClientWithConfig(strfmt.Default, &transport)

	controller := controllers.Controller{
		Config:              config,
		Cache:               cache,
		NitradoServiceToken: envVars.ServiceToken,
		NitradoAPIClient:    nitradoClient,
	}

	routerStruct := routes.Router{
		Controller:   &controller,
		ServiceToken: envVars.ServiceToken,
		Port:         envVars.ListenerPort,
		BasePath:     envVars.BasePath,
	}

	router := routes.GetRouter(ctx)
	routes.AddRoutes(ctx, router, &routerStruct)
}

// InitCache initializes the Redis cache
func InitCache(ctx context.Context, config *configs.Config) *cache.Cache {
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))
	pool, err := cache.GetClient(ctx, config.Redis.Host, config.Redis.Port, config.Redis.Pool)

	if err != nil {
		ctx = logging.AddValues(ctx, zap.NamedError("error", err))
		logger := logging.Logger(ctx)
		logger.Fatal("error_log")
	}

	return &cache.Cache{
		Client: pool,
	}
}

// InitDB initializes the DB connection
func InitDB(ctx context.Context, config *configs.Config) *gorm.DB {
	ctx = logging.AddValues(ctx, zap.String("scope", logging.GetFuncName()))
	gdb, err := db.GetDB(config)

	if err != nil {
		ctx = logging.AddValues(ctx, zap.NamedError("error", err))
		logger := logging.Logger(ctx)
		logger.Fatal("error_log")
	}

	return gdb
}
