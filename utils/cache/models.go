package cache

type Player struct {
	Name   string `json:"name"`
	ID     string `json:"id"`
	IDType string `json:"id_type"`
	Online string `json:"online"`
}

type ServerPlayers struct {
	Players []Player `json:"players"`
}
