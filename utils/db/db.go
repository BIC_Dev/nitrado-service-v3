package db

import (
	"fmt"
	"os"

	"gitlab.com/BIC_Dev/nitrado-service-v3/configs"
	"gorm.io/gorm"
)

// GetDB gets the DB
func GetDB(config *configs.Config) (*gorm.DB, *ModelError) {
	var gdb *gorm.DB

	switch config.DB.Type {
	case "SQLite3":
		SQLite3 := SQLite3{}
		err := SQLite3.Connect(config)

		if err != nil {
			return nil, err
		}

		gdb = SQLite3.DB
	case "PostgreSQL":
		PostgreSQL := PostgreSQL{
			Username: os.Getenv("POSTGRES_USERNAME"),
			Password: os.Getenv("POSTGRES_PASSWORD"),
		}

		err := PostgreSQL.Connect(config)

		if err != nil {
			return nil, err
		}

		gdb = PostgreSQL.DB
	default:
		return nil, &ModelError{
			Err:     fmt.Errorf("Default case hit when instantiating DB"),
			Message: fmt.Sprintf("Invalid DB type: %s", config.DB.Type),
		}
	}

	//gdb.SetLogger(&GormLogger{})
	//gdb.LogMode(true)

	return gdb, nil
}

// Migrate func
func Migrate(gdb *gorm.DB, tables ...interface{}) *ModelError {
	err := gdb.AutoMigrate(tables...)
	if err != nil {
		return &ModelError{
			Message: "Failed to migrate tables",
			Err:     err,
		}
	}

	return nil
}
