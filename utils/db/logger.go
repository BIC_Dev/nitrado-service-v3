package db

import (
	"context"
	"time"

	"gitlab.com/BIC_Dev/nitrado-service-v3/utils/logging"
	"go.uber.org/zap"
	gormlogger "gorm.io/gorm/logger"
)

// GormLogger struct
type GormLogger struct {
	LogLevel      gormlogger.LogLevel
	SlowThreshold time.Duration
}

// NewLogger func
func NewLogger(logLevel gormlogger.LogLevel, slowThreshold time.Duration) *GormLogger {
	return &GormLogger{
		LogLevel:      logLevel,
		SlowThreshold: slowThreshold,
	}
}

// LogMode func
func (l *GormLogger) LogMode(level gormlogger.LogLevel) gormlogger.Interface {
	newLogger := *l
	newLogger.LogLevel = level
	return &newLogger
}

// Info func
func (l *GormLogger) Info(ctx context.Context, msg string, v ...interface{}) {
	ctx = logging.AddValues(ctx,
		zap.String("module", "gorm"),
		zap.String("type", "sql"),
		zap.Int64("rows", v[5].(int64)),
		zap.String("src", v[1].(string)),
		zap.Any("values", v[4]),
		zap.Float64("duration", float64(v[2].(time.Duration).Nanoseconds())/1e6),
		zap.String("query", v[3].(string)),
	)
	logger := logging.Logger(ctx)
	logger.Info("gorm_error_log")
}

// Warn func
func (l *GormLogger) Warn(ctx context.Context, msg string, v ...interface{}) {
	ctx = logging.AddValues(ctx,
		zap.String("module", "gorm"),
		zap.String("type", "sql"),
		zap.Int64("rows", v[5].(int64)),
		zap.String("src", v[1].(string)),
		zap.Any("values", v[4]),
		zap.Float64("duration", float64(v[2].(time.Duration).Nanoseconds())/1e6),
		zap.String("query", v[3].(string)),
	)
	logger := logging.Logger(ctx)
	logger.Warn("gorm_error_log")
}

// Error func
func (l *GormLogger) Error(ctx context.Context, msg string, v ...interface{}) {
	ctx = logging.AddValues(ctx,
		zap.String("module", "gorm"),
		zap.String("type", "sql"),
		zap.Int64("rows", v[5].(int64)),
		zap.String("src", v[1].(string)),
		zap.Any("values", v[4]),
		zap.Float64("duration", float64(v[2].(time.Duration).Nanoseconds())/1e6),
		zap.String("query", v[3].(string)),
	)
	logger := logging.Logger(ctx)
	logger.Error("gorm_error_log")
}

// Trace func
func (l *GormLogger) Trace(ctx context.Context, begin time.Time, fc func() (string, int64), err error) {
	elapsed := time.Since(begin)
	sql, rows := fc()

	switch {
	case err != nil:
		ctx = logging.AddValues(ctx,
			zap.NamedError("error", err),
			zap.Float64("duration", float64(elapsed.Nanoseconds())/1e6),
			zap.String("query", sql),
			zap.Int64("rows", rows),
		)
		logger := logging.Logger(ctx)
		logger.Error("gorm_error_log")
	case elapsed > l.SlowThreshold && l.SlowThreshold != 0:
		ctx = logging.AddValues(ctx,
			zap.Float64("duration", float64(elapsed.Nanoseconds())/1e6),
			zap.String("query", sql),
			zap.Int64("rows", rows),
		)
		logger := logging.Logger(ctx)
		logger.Warn("gorm_slow_log")
	default:
		ctx = logging.AddValues(ctx,
			zap.Float64("duration", float64(elapsed.Nanoseconds())/1e6),
			zap.String("query", sql),
			zap.Int64("rows", rows),
		)
		logger := logging.Logger(ctx)
		logger.Debug("gorm_query_log")
	}
}

// // Print func
// func (*GormLogger) Print(v ...interface{}) {
// 	ctx := context.Background()

// 	switch v[0] {
// 	case "sql":
// 		ctx = logging.AddValues(ctx,
// 			zap.String("module", "gorm"),
// 			zap.String("type", "sql"),
// 			zap.Int64("rows", v[5].(int64)),
// 			zap.String("src", v[1].(string)),
// 			zap.Any("values", v[4]),
// 			zap.Float64("duration", float64(v[2].(time.Duration).Nanoseconds())/1e6),
// 			zap.String("query", v[3].(string)),
// 		)
// 		logger := logging.Logger(ctx)
// 		logger.Debug("gorm_query_log")
// 	default:
// 		ctx = logging.AddValues(ctx,
// 			zap.NamedError("error", v[2].(error)),
// 		)
// 		logger := logging.Logger(ctx)
// 		logger.Info("gorm_error_log")
// 	}
// }
