package db

import (
	"fmt"

	"gitlab.com/BIC_Dev/nitrado-service-v3/configs"
	"gorm.io/gorm"

	// Loading Postgres dialect
	"gorm.io/driver/postgres"
)

// PostgreSQL struct
type PostgreSQL struct {
	DB       *gorm.DB
	Username string
	Password string
}

// Connect starts initial connection to DB
func (pg *PostgreSQL) Connect(c *configs.Config) *ModelError {
	connectionString := fmt.Sprintf("host=%s port=%s user=%s dbname=%s password=%s", c.PostgreSQL.Host, c.PostgreSQL.Port, pg.Username, c.PostgreSQL.DBName, pg.Password)
	logger := NewLogger(0, 0)
	newDB, err := gorm.Open(postgres.Open(connectionString), &gorm.Config{
		Logger: logger,
	})

	if err != nil {
		return &ModelError{
			Err:     err,
			Message: "Failed to connect to PostgreSQL DB",
		}
	}

	pg.DB = newDB

	return nil
}

// Migrate migrates a table
func (pg *PostgreSQL) Migrate(table interface{}) *ModelError {
	pg.DB.AutoMigrate(table)

	return nil
}

// GetDB gets the DB from the struct
func (pg *PostgreSQL) GetDB() *gorm.DB {
	return pg.DB
}
